﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class move : MonoBehaviour {
	
	public Text scoretxt;
	private float score;
	public Text winTxt;

	void Start(){
		winTxt.text = "";
	}

	// Update is called once per frame
	void Update () {
		scoretxt.text = score.ToString("Cubes picked: " + "0");

		if(score >= 10){
			print ("you win!");
			winTxt.text = "YOU WIN!";
		}

		if(Input.GetKey(KeyCode.UpArrow)){
			print ("up");
			GetComponent<Rigidbody>().AddForce(Vector3.forward * 5);
		}

		if(Input.GetKey(KeyCode.DownArrow)){
			print("down");
			GetComponent<Rigidbody>().AddForce(Vector3.back * 5);
		}

		if(Input.GetKey(KeyCode.RightArrow)){
			print("right");
			GetComponent<Rigidbody>().AddForce(Vector3.right * 5);
		}

		if(Input.GetKey(KeyCode.LeftArrow)){
			print("left");
			GetComponent<Rigidbody>().AddForce(Vector3.left * 5);
		}
	}

	void OnTriggerEnter (Collider other) {
		if(other.gameObject.CompareTag("collectable")){
			Destroy(other.gameObject);
			score++;
		}
	}
}
